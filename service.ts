import store from "./store";
import GameDefinition from "boardie/dist/GameDefinition";
import Game from "boardie/dist/Game";
import Decision from "boardie/dist/Decision";
import Choice from "boardie/dist/choices/Choice";

export function getGameDefinitions(): Promise<any> {
    return fetch("/gameDefinitions")
        .then(result => result.json())
        .then(gameDefinitions => store.gameDefinitions = gameDefinitions);
}

export function getGames(): Promise<any> {
    return fetch("/games")
        .then(result => result.json())
        .then(games => store.games = games);
}

export function createNewGame(gameDefinition: GameDefinition): void {
    store.ws!.send(JSON.stringify({event:"newGame", gameName: gameDefinition.name, playerName: store.playerName}));
}

export function addUserToGame(game: Game, playerName: string, ai: boolean): void {
    store.ws!.send(JSON.stringify({event: "join", gameId: game.data.id, playerName, ai}));
}

export function joinGame(game: Game): void {
    addUserToGame(game, store.playerName, false);
    store.game = store.games.find(g => g.data.id == game.data.id);
}

export function startGame(game: Game): Promise<any> {
    return fetch(`/games/${game.data.id}/start`, {method: "POST"});
}

export function makeDecision(decision: Decision, choice: Choice): void {
    store.ws!.send(JSON.stringify({event: "makeDecision", decisionId: decision.id, choiceId: choice.id}));
    store.decision = undefined; // TODO should probably wait for the server
}