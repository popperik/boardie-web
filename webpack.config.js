const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
    mode: "development",
    entry: "./index.ts",
    module: {
        rules: [
            {   // Typescript
                test: /\.ts$/,
                use: {
                    loader: "ts-loader",
                    options: { // Needed so we can just import .vue files
                        appendTsSuffixTo: [/\.vue$/]
                    }
                }
            },
            {   // Vue
                test: /\.vue$/,
                loader: "vue-loader"
            },
            {   // CSS (for bootstrap)
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    plugins: [new VueLoaderPlugin()],
    resolve: {
        extensions: [".vue", ".js", ".ts"]
    },
    output: {
        filename: "bundle.js",
        path: `${__dirname}/dist`
    }
};