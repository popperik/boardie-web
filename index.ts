import Vue from "vue";
import VueRouter from "vue-router";
import { BootstrapVue } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import main from "./main";
import Index from "./views/Index";
import GamePage from "./views/GamePage";

Vue.use(VueRouter);
Vue.use(BootstrapVue);

const router = new VueRouter({
    routes: [
        { path: "/", component: Index },
        { path: "/gamePage/:gameId", component: GamePage }
    ]
});

const app = new Vue({
    el: "#app",
    router,
    render: h => h(main)
});