import GameDefinition from "boardie/dist/GameDefinition";
import Game from "boardie/dist/Game";
import Decision from "boardie/dist/Decision";

export interface BoardieState {
    decision: Decision | undefined,
    gameDefinitions: GameDefinition[],
    games: Game[],
    game: Game | undefined,
    gameInProgress: boolean,
    playerName: string,
    logEntries: string[],
    ws: WebSocket | undefined
}

const store: BoardieState = {
    decision: undefined,
    gameDefinitions: [],
    game: undefined,
    games: [],
    gameInProgress: false,
    playerName: "Player",
    logEntries: [],
    ws: undefined
}

export default store;